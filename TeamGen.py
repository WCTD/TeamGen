#!/usr/bin/python3
import pdb
from datetime import datetime as dt

def getTeams(fileName,numTeams = 4):
    studentArr = open(fileName,'r').read().split("\n")
    for i,val in enumerate(studentArr):
        studentArr[i] = val.split(",")
    studentArr = studentArr[1:len(studentArr)-1]
    students = []
    for i in studentArr:
        #students.append(Student(int(i[0]),int(i[2]),i[2],int(i[3]),int(i[4]),int(i[5])))
        name = i[0].strip() + " " + i[1].strip()
        school = i[2].lower()
        DOB = i[3].split("/")
        DOB = dt(year=int(DOB[2]),month=int(DOB[0]),day=int(DOB[1]))
        age = int((dt.now() - DOB).total_seconds() / 31557600)
        gender = 0
        if i[4] == "Female":
            gender = 1
        code = int(i[5])
        art = int(i[6])
        stude = Student(name, gender, school, age, code, art)
        students.append(stude)
    teams = []
    for _ in range(numTeams):
        teams.append(Team())
    numStudes = len(students)
    #for i in range(int(numStudes/numTeams)):
    while (len(students) != 0): 
    #print("For run number " + str(i+1))
        for ii in range(len(teams)):
            team = teams[ii]
            #print("In team " + str(ii+1))
            mostDiff = team.findStudent(students)
            #print("The most different student was:")
            diffStude = students[mostDiff]
            #print("Student #" + str(mostDiff) + ", " + str(diffStude))
            #print("The students of team " + str(ii + 1) + " are currently")
            #print(len(team.students))
            team.add(diffStude)
            #print("They are now:")
            #print(len(team.students))
            students.remove(diffStude)
            #pdb.set_trace()
            if (len(students) == 0):
                break
    for i,t in enumerate(teams):
        print("Team # " + str(i+1) + ":")
        print(str(t) + "\n")
    if (len(students) != 0):
        print("The following students did not fit into a team:")
        for s in students:
            print(s)

class Student:
    weights = [5,4,3,2,1]
    def __init__(self, nameI,genderI, schoolI, ageI, codeI, artI):
        self.name = nameI
        self.gender = genderI#1 for a woman, 0 for a male
        self.school = schoolI
        self.age = ageI
        self.code = codeI
        self.art = artI

    def diff(self, otherStude):
        difference = 0
        if (self.gender != otherStude.gender):
            difference += Student.weights[0]
        if (self.school != otherStude.school):
            difference += Student.weights[1]
        difference += (abs(self.age - otherStude.age)/5) * Student.weights[2]
        difference += (abs(self.code - otherStude.code)/2) * Student.weights[3]
        difference += (abs(self.art - otherStude.art)/2) * Student.weights[4]
        return difference
    
    def __str__(self):
        return self.name

class Team:
    def __init__(self):
        self.students = []
    def __getAverageStudent__(self):
        if len(self.students) == 0:
            return None
        avgGender = 0
        for i in self.students:
            avgGender += i.gender
        avgGender = int(avgGender/len(self.students))
        schools = []
        population = []
        for i in self.students:
            if (i.school in schools):
                population[schools.index(i.school)] += 1
            else:
                schools.append(i.school)
                population.append(1)
        avgSchool = schools[population.index(max(population))]
        avgAge = 0
        avgCodes = 0
        avgArts = 0
        for i in self.students:
            avgAge += i.age
            avgCodes += i.code
            avgArts += i.art
        length = len(self.students)
        avgAge /= length
        avgCodes /= length
        avgArts /= length
        return Student("AvgStudent",avgGender,avgSchool,avgAge,avgCodes,avgArts)
    def add(self,student):
        self.students.append(student)

    def findStudent(self,pool):
        avg = self.__getAverageStudent__()
        maxI = 0
        maxDiff = -1
        if (len(self.students) == 0):
            return 0
        for i,val in enumerate(pool):
            if (val.diff(avg) > maxDiff):
                maxI = i
                maxDiff = val.diff(avg)
        return maxI

    def __str__(self):
        if (len(self.students) == 0): return "Team with no students"
        outStr = ""
        for i in self.students:
            outStr += str(i.name) + ","
        return outStr[:len(outStr)-1]

if __name__ == "__main__":
    print("\nInput file name (file must be in the same directory):")
    fileName = input("File Name:").strip()
    print("Input number of teams:")
    numTeams = int(input("Number of teams:").strip())
    print("\n")
    getTeams(fileName,numTeams)
