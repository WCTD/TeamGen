# TeamGen

A team generator using python 3.

## Installation

Your machine requires python 3 to run this code properly. You can download that [here](https://www.python.org/).
You can download this project using the green button above and hitting the "Download ZIP button".
Unzip this file to a directory.

## Usage

To parse a .csv file, place it in the unzipped directory and run the 'TeamGen.py' file with python 3. The program should instruct you how to choose the right csv file and the number of groups.
